#include <iostream>
#include <string>

class Doubly_linked_list
{
public:
    Doubly_linked_list()
    {
        head = nullptr;
        tail = nullptr;
        length = 0;
    }
    void add(int val, int i)
    {
        if (i < 0)
        {
            throw std::exception("Index is negative!");
        }
        Node* node = new Node();
        node->value = val;
        length++;
        if (head == nullptr)
        {
            head = node;
            tail = node;
            return;
        }
        int count = 0;
        Node* current = head;
        while (current->next != nullptr && i > count + 1)
        {
            current = current->next;
            count++;
        }
        if (current->next == nullptr && i > count)
        {
            node->next = current->next;
            node->prev = current;
            current->next = node;
            tail = node;
            return;
        }
        node->next = current->next;
        node->prev = current->next->prev;
        current->next = node;
        node->next->prev = node;
    }
    void pop()
    {
        if (is_empty())
        {
            throw std::exception("List is empty!");
        }
        Node* current = head;
        length--;
        if (current->next == nullptr)
        {
            Node* node = current;
            delete node;
            head = nullptr;
            tail = nullptr;
            return;
        }
        while (current->next->next != nullptr)
        {
            current = current->next;
        }
        tail = current;
        Node* node = current->next;
        current->next->prev = nullptr;
        current->next = nullptr;
        delete node;
    }
    void pop(int i)
    {
        if (i < 0)
        {
            throw std::exception("Index is negative!");
        }
        if (is_empty())
        {
            throw std::exception("List is empty!");
        }
        Node* current = head;
        int count = 1;
        while (current->next != nullptr && i > count)
        {
            current = current->next;
            count++;
        }
        if (current->next == nullptr && i > count)
        {
            throw std::exception("Index is out of range!");
        }
        length--;
        if (head->next == nullptr)
        {
            Node* node = head;
            head = nullptr;
            tail = nullptr;
            delete node;
            return;
        }
        if (current->next->next == nullptr)
        {
            tail = current;
            Node* node = current->next;
            current->next->prev = nullptr;
            current->next = nullptr;
            delete node;
            return;
        }
        Node* node = current->next;
        current->next = node->next;
        node->next->prev = current;
        node->next = nullptr;
        node->prev = nullptr;
        delete node;
    }
    int at(int i)
    {
        if (i < 0)
        {
            throw std::exception("Index is negative!");
        }
        if (i > length - 1)
        {
            throw std::exception("Index is out of range!");
        }
        if (head == nullptr)
        {
            throw std::exception("List is empty!");
        }
        Node* current = nullptr;
        if (i < length / 2)
        {
            current = head;
            int count = 0;
            while (i > count)
            {
                current = current->next;
                count++;
            }
        }
        else
        {
            current = tail;
            int count = length - 1;
            while (i < count)
            {
                current = current->prev;
                count--;
            }
        }
        return current->value;
    }
    bool is_empty()
    {
        if (head == nullptr)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    void print()
    {
        if (is_empty())
        {
            std::cout << "List is empty." << std::endl;
            return;
        }
        Node* current = head;
        while (current->next != nullptr)
        {
            std::cout << current->value << " ";
            current = current->next;
        }
        std::cout << current->value << std::endl;
    }
private:
    struct Node
    {
        int value;
        Node* next;
        Node* prev;
    };
    Node* head;
    Node* tail;
    int length;
};

int main()
{
    Doubly_linked_list* lst = new Doubly_linked_list();
    std::string str;
    std::cout << "Doubly Linked List Menu" << std::endl << "1. add value at index" << std::endl << "2. delete value at the end" << std::endl << "3. delete value at index"
        << std::endl << "4. value at index" << std::endl << "5. check if list is empty" << std::endl << "6. print list" <<
        std::endl << "7. end programme" << std::endl;
    do
    {
        std::getline(std::cin, str);
        if (str == "add value at index")
        {
            int val;
            int i;
            std::cin >> val >> i;
            try
            {
                lst->add(val, i);
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "delete value at the end")
        {
            try
            {
                lst->pop();
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "delete value at index")
        {
            int i;
            std::cin >> i;
            try
            {
                lst->pop(i);
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "value at index")
        {
            int i;
            std::cin >> i;
            try
            {
                std::cout << lst->at(i);
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "check if list is empty")
        {
            if (lst->is_empty())
            {
                std::cout << "List is empty.";
            }
            else
            {
                std::cout << "List is not empty.";
            }
        }
        else if (str == "print list")
        {
                lst->print();
        }
    }     while (str != "end programme");
    system("pause");
}